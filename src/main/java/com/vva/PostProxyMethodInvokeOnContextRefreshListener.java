package com.vva;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author vystupkin
 */
@Component
public class PostProxyMethodInvokeOnContextRefreshListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    ConfigurableListableBeanFactory factory;

    @Override
    @SneakyThrows
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        ApplicationContext context = contextRefreshedEvent.getApplicationContext();
        for (String beanName : context.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = factory.getBeanDefinition(beanName);
            if (Objects.isNull(beanDefinition.getBeanClassName())) {
                continue;
            }
            Class<?> originalType = Class.forName(beanDefinition.getBeanClassName());
            List<Method> postProxyMethods = Stream.of(originalType.getMethods()).filter(m -> m.isAnnotationPresent(PostProxy.class)).collect(Collectors.toList());
            if (!postProxyMethods.isEmpty()) {
                Object bean = context.getBean(beanName);
                Class<?> finalType = bean.getClass();

                postProxyMethods.forEach(m -> {
                    try {
                        Method finalMethod = finalType.getMethod(m.getName());
                        finalMethod.invoke(bean);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }
}
