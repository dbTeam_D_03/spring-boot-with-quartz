package com.vva;

import lombok.SneakyThrows;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author vystupkin
 */
@Component
public class A {

    @Autowired
    Scheduler scheduler;

    @PostConstruct
    public void init() {
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    }


    @PostProxy
    @SneakyThrows
    public void postProxy() {

        System.out.println("BB");

        List<String> recipients = Arrays.asList("vva@mail.com","ddd@mail.com");
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("value", "hello");
        jobDataMap.put("recipients",recipients);
        JobDetail jobDetail = JobBuilder.newJob(SimpleJobBean.class)
                .usingJobData(jobDataMap).build();


        Trigger trigger = TriggerBuilder
                .newTrigger()
                .startAt(new Date(new Date().getTime() + 60 * 1000))
                .build();
        scheduler.scheduleJob(jobDetail,trigger);
    }

}
