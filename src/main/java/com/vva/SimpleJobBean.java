package com.vva;

import lombok.Setter;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.List;

/**
 * @author vystupkin
 */
@Setter
public class SimpleJobBean extends QuartzJobBean {


    private String value;

    private List<String> recipients;


    @Override
    protected void executeInternal(JobExecutionContext arg0)
            throws JobExecutionException {
        System.out.println("********JOB started*******");
        recipients.forEach(recipient-> System.out.println("Sending "+value+" to "+recipient));
        System.out.println("********JOB ended*******");
    }


}
